<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Chocolate For Good | @yield('title', '')</title>

        <link href="/img/favicon.ico" rel="SHORTCUT ICON" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

        @yield('extra-css')
        <style>
                .bd-placeholder-img {
                    font-size: 1.125rem;
                    text-anchor: middle;
                }
        
                {{--  .active {
                    color: white !important;
                }  --}}
        
                @media (min-width: 768px) {
                    .bd-placeholder-img-lg {
                        font-size: 3.5rem;
                    }
                }
        
                .dropbtn {
                   {{--  color: #999 !important;  --}}
                    transition: ease-in-out color .15s;
                    padding: 16px;
                    font-size: 16px;
                    border: none;
                    cursor: pointer;
                }
        
                .dropdown {
                    float: right;
                    position: relative;
                    display: inline-block;
                }
        
                .dropdown-content {
                    display: none;
                    position: absolute;
                    background-color: #5c5c5c;
                    min-width: 250px;
                    overflow: auto;
                    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
                    right: 0;
                    z-index: 1;
                    font-size: 15px;
                }
        
                .dropdown-content a {
                    color: black;
                    padding: 12px 16px;
                    text-decoration: none;
                    display: block;
                }
                .dropdown-content a:hover {
                    color: #fff !important;
                    text-decoration: none !important;
                    background: #3987f7 !important;
                }
        
                .site-header a:hover {
                    color: #fff !important;
                    text-decoration: none !important;
                }
        
                .show {
                    display: block;
                }
            </style>
    </head>


<body class="@yield('body-class', '')">
    @include('partials.nav')

    @yield('content')

    @include('partials.footer')

    @yield('extra-js')
    <script>
            /* When the user clicks on the button,
                toggle between hiding and showing the dropdown content */
            function myFunction() {
                document.getElementById("myDropdown").classList.toggle("show");
            }
    
            // Close the dropdown if the user clicks outside of it
            window.onclick = function (event) {
                if (!event.target.matches('.dropbtn')) {
                    var dropdowns = document.getElementsByClassName("dropdown-content");
                    var i;
                    for (i = 0; i < dropdowns.length; i++) {
                        var openDropdown = dropdowns[i];
                        if (openDropdown.classList.contains('show')) {
                            openDropdown.classList.remove('show');
                        }
                    }
                }
            }
        </script>
</body>
</html>
