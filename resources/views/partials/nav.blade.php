<header>
    <div class="top-nav container">
        <div class="logo"><a href="/">Chocolate For Good</a></div>
        @if (! request()->is('checkout'))
        <ul>
            <li><a href="{{ route('shop.index') }}">Shop</a></li>
            <li><a href="#">About</a></li>
            <div class="dropdown">
                <a class="py-2 d-none d-md-inline-block dropbtn" onclick="myFunction()">Products</a>
                <div id="myDropdown" class="dropdown-content">
                    <a class="dropdown-item" href="shop?category=golden-tree">Golden Tree</a>
                        <a class="dropdown-item" href="shop?category=niche-chocolate">Niche Chocolate</a>
                        <a class="dropdown-item" href="shop?category=Toblerone">Tobolerona</a>
                        <a class="dropdown-item" href="shop?category=ferrero-rocher">Ferrero Rocher</a>
                        <a class="dropdown-item" href="shop?category=nestle-chocolate">Nestle Chocolate</a>
                        <a class="dropdown-item" href="shop?category=cadbury-chocolate">Cadbury Chocolate</a>
                </div>
            </div>
            {{--  <li><a href="#">Products</a></li>  --}}
            <li>
                <a href="{{ route('cart.index') }}">Cart <span class="cart-count">
                    @if (Cart::instance('default')->count() > 0)
                    <span>{{ Cart::instance('default')->count() }}</span></span>
                    @endif
                </a>
            </li>
        </ul>
        @endif
    </div> <!-- end top-nav -->
</header>
