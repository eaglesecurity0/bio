<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Laptops
        for ($i = 1; $i <= 30; $i++) {
            Product::create([
                'name' => 'Golden Tree ' . $i,
                'slug' => 'golden-tree-' . $i,
                'details' => 'Kingsbite. Kingsbite is a milk chocolate designed to melt delightfully in the mouth with a smooth release of all the rich goodness of cocoa and milk.',
                'price' => rand(15, 20),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(1);
        }

        // Make laptop have a category of "Desktop" as well.
        // $product = Product::find(1);
        // $product->categories()->attach(2);

        // Desktops
        for ($i = 1; $i <= 9; $i++) {
            Product::create([
                'name' => 'Niche Chocolate ' . $i,
                'slug' => 'niche-chocolate-' . $i,
                'details' => 'Creamy milk chocolate with rich caramel notes and a strong cocoa flavour',
                'price' => rand(20, 30),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(2);
        }

        // Phones
        for ($i = 1; $i <= 9; $i++) {
            Product::create([
                'name' => 'Toblerone ' . $i,
                'slug' => 'toblerone-' . $i,
                'details' => 'Toblerone is made from some of the finest raw materials from around the world. Reawaken your taste buds and immerse yourself in the Toblerone unique taste experience',
                'price' => rand(35, 50),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(3);
        }

        // Tablets
        for ($i = 1; $i <= 9; $i++) {
            Product::create([
                'name' => 'Ferrero Rocher ' . $i,
                'slug' => 'ferrero-rocher-' . $i,
                'details' => 'The iconic original. A whole hazelnut, dipped in smooth chocolaty cream, wrapped in a crispy wafer coated in milk chocolate and covered in hazelnut pieces. Discover Ferrero Rocher, an experience like no other.',
                'price' => rand(35, 50),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(4);
        }

        // TVs
        for ($i = 1; $i <= 9; $i++) {
            Product::create([
                'name' => 'Nestle Chocolate ' . $i,
                'slug' => 'nestle-chocolate-' . $i,
                'details' => 'Nestle Organic Dark Chocolate Morsels. With no artificial colors, flavors or preservatives, your baked goods might just be a little bit better for you. And with only 80 calories per tablespoon,',
                'price' => rand(15, 30),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(5);
        }

        // Cameras
        for ($i = 1; $i <= 9; $i++) {
            Product::create([
                'name' => 'Cadbury Chocolate ' . $i,
                'slug' => 'cadbury-chocolate-' . $i,
                'details' => 'Make any moment more delicious with this premium milk chocolate candy bar, made with creamy CADBURY Chocolate.',
                'price' => rand(25, 40),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(6);
        }

        // Appliances
        // for ($i = 1; $i <= 9; $i++) {
        //     Product::create([
        //         'name' => 'Appliance ' . $i,
        //         'slug' => 'appliance-' . $i,
        //         'details' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, dolorum!',
        //         'price' => rand(79999, 149999),
        //         'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
        //     ])->categories()->attach(7);
        // }

        // Select random entries to be featured
        Product::whereIn('id', [1, 12, 22, 31, 41, 43, 47, 51, 53, 61, 69, 73, 80])->update(['featured' => true]);
    }
}
