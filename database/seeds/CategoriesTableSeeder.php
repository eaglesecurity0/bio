<?php

use App\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Category::insert([
            ['name' => 'Golden Tree', 'slug' => 'golden-tree', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Niche Chocolate', 'slug' => 'niche-chocolate', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Toblerone', 'slug' => 'Toblerone', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Ferrero Rocher', 'slug' => 'ferrero-rocher', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Nestle Chocolate', 'slug' => 'nestle-chocolate', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Cadbury Chocolate', 'slug' => 'cadbury-chocolate', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
