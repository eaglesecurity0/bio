<?php

function presentPrice($price)
{
    // return money_format('GH₵%i', $price / 1);
    return 'GH₵'.number_format($price / 1, 2);
}

function setActiveCategory($category, $output = 'active')
{
    return request()->category == $category ? $output : '';
}
